﻿
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using SBCodeTest.Storage;
using Storage.Models;

namespace MVXTest.ViewModels
{
    public class EmployeeDetailsViewModel : MvxViewModel<EmployeeModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private EmployeeModel _model;
        private bool _readyToSave;
        private bool _canDelete;

        private const string _doneSvgIcon = @"<svg  width=""100"" height=""100"" viewBox=""0 0 24 24"" ><path fill=""white"" d=""M17.3 6.3c-.39-.39-1.02-.39-1.41 0l-5.64 5.64 1.41 1.41L17.3 7.7c.38-.38.38-1.02 0-1.4zm4.24-.01l-9.88 9.88-3.48-3.47c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L22.95 7.71c.39-.39.39-1.02 0-1.41h-.01c-.38-.4-1.01-.4-1.4-.01zM1.12 14.12L5.3 18.3c.39.39 1.02.39 1.41 0l.7-.7-4.88-4.9c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.03 0 1.42z"" /></svg>";
        private const string _undoneSvgIcon = @"<svg  width=""100"" height=""100"" viewBox=""0 0 24 24"" > <path fill = ""BB171A"" d=""M12 7c.55 0 1 .45 1 1v4c0 .55-.45 1-1 1s-1-.45-1-1V8c0-.55.45-1 1-1zm-.01-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm1-3h-2v-2h2v2z"" /></ svg > ";

        private const string LengthDone = "5 to 12 digits long - DONE";
        private const string LengthUndone = "5 to 12 digits long - REQUIRED";

        private const string RepeatDone = "No repeating character sequences - DONE";
        private const string RepeatUndone = "No repeating character sequences - REQUIRED";

        private const string NoSpecialDone = "No special characters - DONE";
        private const string NoSpecialUndone = "No special characters - REQUIRED";

        private const string NumAndLetLabelDone = "Minumum 1 letter and 1 number - DONE";
        private const string NumAndLetLabelUndone = "Minumum 1 letter and 1 number - REQUIRED";

        private const string MatchDone = "Passwords must match - DONE";
        private const string MatchUndone = "Passwords must match - REQUIRED";

        private string _userName;
        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
                ValidatePasswords();
            }
        }

        private string _passwordConfirm;
        public string PasswordConfirm
        {
            get => _passwordConfirm;
            set
            {
                _passwordConfirm = value;
                RaisePropertyChanged(() => PasswordConfirm);
                ValidatePasswords();
            }
        }

        private string _lengthLabelText;
        public string LengthLabelText
        {
            get => _lengthLabelText;
            set
            {
                _lengthLabelText = value;
                RaisePropertyChanged(() => LengthLabelText);
            }
        }

        private string _noRepeatLabelText;
        public string NoRepeatLabelText
        {
            get => _noRepeatLabelText;
            set
            {
                _noRepeatLabelText = value;
                RaisePropertyChanged(() => NoRepeatLabelText);
            }
        }

        private string _noSpecialLabelText;
        public string NoSpecialLabelText
        {
            get => _noSpecialLabelText;
            set
            {
                _noSpecialLabelText = value;
                RaisePropertyChanged(() => NoSpecialLabelText);
            }
        }

        private string _numAndLetLableText;
        public string NumAndLetLableText
        {
            get => _numAndLetLableText;
            set
            {
                _numAndLetLableText = value;
                RaisePropertyChanged(() => NumAndLetLableText);
            }
        }

        private string _matchLableText;
        public string MatchLableText
        {
            get => _matchLableText;
            set
            {
                _matchLableText = value;
                RaisePropertyChanged(() => MatchLableText);
            }
        }

        private MvvmCross.Commands.MvxCommand _saveEmployeeCommand;
        public System.Windows.Input.ICommand SaveEmployeeCommand
        {
            get
            {
                _saveEmployeeCommand = _saveEmployeeCommand ?? new MvvmCross.Commands.MvxCommand(SaveEmployee);
                return _saveEmployeeCommand;
            }
        }

        private MvvmCross.Commands.MvxCommand _deleteEmployeeCommand;
        public System.Windows.Input.ICommand DeleteEmployeeCommand
        {
            get
            {
                _deleteEmployeeCommand = _deleteEmployeeCommand ?? new MvvmCross.Commands.MvxCommand(new Action(() =>
                {
                    if (_canDelete)
                    {
                        LocalAppStore.Instance.DeleteItemAsync(_model);
                        _navigationService.Close(this);
                    }
                }));
                return _deleteEmployeeCommand;
            }
        }

        private void SaveEmployee()
        {
            if (_readyToSave)
            {
                Task.Run(async () =>
                {
                    await LocalAppStore.Instance.SaveItemAsync(new EmployeeModel { Name = UserName, Password = Password });
                    await _navigationService.Close(this);
                });
            }
        }

        public EmployeeDetailsViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void Prepare()
        {
            // first callback. Initialize parameter-agnostic stuff here
        }

        public override void Prepare(EmployeeModel parameter)
        {
            _model = parameter;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            LengthLabelText = LengthUndone;
            NoRepeatLabelText = RepeatUndone;
            NoSpecialLabelText = NoSpecialUndone;
            NumAndLetLableText = NumAndLetLabelUndone;
            MatchLableText = MatchUndone;

            if (string.IsNullOrEmpty(_model.Name) == false)
            {
                UserName = _model.Name;
                Password = _model.Password;
                PasswordConfirm = _model.Password;
                _canDelete = true;
            }
            else
            {
                _canDelete = false;
                Password = string.Empty;
                PasswordConfirm = string.Empty;
                UserName = string.Empty;
            }

            ValidatePasswords();
        }

        private void ValidatePasswords()
        {
            _readyToSave = true;

            // check password length
            var isLength = Regex.IsMatch(Password, @"^\w{5,12}$");
            if (isLength)
            {
                LengthLabelText = LengthDone;
            }
            else
            {
                LengthLabelText = LengthUndone;
                _readyToSave = false;
            }

            // check for minumum requires
            bool containsInt = Password.Any(char.IsDigit);
            bool containsLet = Password.Any(char.IsLetter);
            if (containsInt && containsLet)
            {
                NumAndLetLableText = NumAndLetLabelDone;
            }
            else
            {
                NumAndLetLableText = NumAndLetLabelUndone;
                _readyToSave = false;
            }

            // check for invalid chars
            if (Regex.IsMatch(Password, @"^[a-zA-Z0-9]+$"))
            {
                NoSpecialLabelText = NoSpecialDone;
            }
            else
            {
                NoSpecialLabelText = NoSpecialUndone;
                _readyToSave = false;
            }

            var isRepeting = Regex.IsMatch(Password, @"^.*(?<grp>[a-z0-9]+)(\k<grp>).*$");
            if (isRepeting)
            {
                NoRepeatLabelText = RepeatUndone;
                _readyToSave = false;
            }
            else
            {
                NoRepeatLabelText = RepeatDone;
            }

            // check if passwords match
            if (string.IsNullOrEmpty(Password) == false
                && string.IsNullOrEmpty(PasswordConfirm) == false
                && Password == PasswordConfirm)
            {
                MatchLableText = MatchDone;
            }
            else
            {
                MatchLableText = MatchUndone;
                _readyToSave = false;
            }

            if (string.IsNullOrEmpty(UserName))
            {
                _readyToSave = false;
            }

            Console.WriteLine(Password);
            Console.WriteLine(PasswordConfirm);
        }
    }
}
