﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using SBCodeTest.Storage;
using Storage.Models;

namespace MVXTest.ViewModels
{
    public class EmployeeListViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public EmployeeListViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            Items = new List<EmployeeModel>();
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private bool _isEmptyListViewHidden;
        public bool IsEmptyListViewHidden
        {
            get { return _isEmptyListViewHidden; }
            set { _isEmptyListViewHidden = value; RaisePropertyChanged(() => IsEmptyListViewHidden); }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; RaisePropertyChanged(() => IsLoading); }
        }

        private List<EmployeeModel> _items;
        public List<EmployeeModel> Items
        {
            get { return _items; }
            set { _items = value; RaisePropertyChanged(() => Items); }
        }

        public override void Start()
        {
            LoadData();
        }

        private void LoadData()
        {
            IsLoading = true;
            IsEmptyListViewHidden = false;

            Task.Run(async () =>
            {
                var items = await LocalAppStore.Instance.GetItemsAsync();
                Items = items;

                if (Items.Count > 0)
                {
                    IsEmptyListViewHidden = false;
                }

                IsLoading = false;
            });
        }

        private MvvmCross.Commands.MvxCommand _reloadDataCommand;
        public System.Windows.Input.ICommand ReloadDataCommand
        {
            get
            {
                _reloadDataCommand = _reloadDataCommand ?? new MvvmCross.Commands.MvxCommand(LoadData);
                return _reloadDataCommand;
            }
        }

        private MvvmCross.Commands.MvxCommand _addNewEmployeeCommand;
        public System.Windows.Input.ICommand AddNewEmployeeCommand
        {
            get
            {
                _addNewEmployeeCommand = _addNewEmployeeCommand ?? new MvvmCross.Commands.MvxCommand(NavigateToNewEmployeeDetailAsync);
                return _addNewEmployeeCommand;
            }
        }

        private MvvmCross.Commands.MvxCommand<EmployeeModel> _itemSelectedCommand;
        public System.Windows.Input.ICommand ItemSelectedCommand
        {
            get
            {
                _itemSelectedCommand = _itemSelectedCommand ?? new MvvmCross.Commands.MvxCommand<EmployeeModel>(DoSelectItem);
                return _itemSelectedCommand;
            }
        }

        private void DoSelectItem(EmployeeModel item)
        {
            _navigationService.Navigate<EmployeeDetailsViewModel, EmployeeModel>(item);
        }

        private void NavigateToNewEmployeeDetailAsync()
        {
            _navigationService.Navigate<EmployeeDetailsViewModel, EmployeeModel>(new EmployeeModel());
        }
    }
}



