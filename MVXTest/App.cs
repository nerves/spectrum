﻿
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.UI;
using MvvmCross.ViewModels;
using MVXTest.ViewModels;
using SBCodeTest.Storage;

namespace MVXTest
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            this.CreatableTypes()
              .EndingWith("Service")
              .AsInterfaces()
              .RegisterAsLazySingleton();

            var ignore = LocalAppStore.Instance;

            Mvx.IoCProvider.RegisterType<IMvxNativeColor>();

            RegisterAppStart<EmployeeListViewModel>();
        }
    }
}
