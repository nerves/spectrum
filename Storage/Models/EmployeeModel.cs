﻿
using SQLite;

namespace Storage.Models
{
    public class EmployeeModel
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }

        public string Name { get; set; }
        public string Password { get; set; }
    }
}
