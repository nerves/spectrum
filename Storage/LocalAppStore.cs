﻿
using System;
using System.IO;
using SQLite;
using System.Threading.Tasks;
using System.Collections.Generic;
using Storage.Models;

namespace SBCodeTest.Storage
{
    public class LocalAppStore
    {
        private string _databasePath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AppStorage.db");
            }
        }

        private SQLiteAsyncConnection _connection;

        private static LocalAppStore _instance;
        public static LocalAppStore Instance => _instance ?? (_instance = new LocalAppStore());

        #region Life Cycle

        public LocalAppStore()
        {
            Task.Run(async () =>
            {
                try
                {
                    _connection = new SQLiteAsyncConnection(_databasePath);

                    await _connection.CreateTableAsync<EmployeeModel>();

                    if (await _connection.Table<EmployeeModel>().Where(x => x.Name == "Jason Nesmith").CountAsync() == 0)
                    {
                        await _connection.InsertAsync(new EmployeeModel { Name = "Jason Nesmith", Password = "Neverg1veup" });
                        await _connection.InsertAsync(new EmployeeModel { Name = "Buzz Lightyear", Password = "2infinity" });
                        await _connection.InsertAsync(new EmployeeModel { Name = "Buzz Aldrin", Password = "1smallstep" });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"There was an error {e}");
                }
            });
        }

        public static void Initialize()
        {
            var conn = Instance;
        }

        #endregion

        #region Access 


        public async Task<List<EmployeeModel>> GetItemsAsync()
        {
            var stuff = await _connection.Table<EmployeeModel>().ToListAsync();
            return stuff;
        }

        public Task<EmployeeModel> GetItemAsync(long id)
        {
            return _connection.Table<EmployeeModel>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> SaveItemAsync(EmployeeModel item)
        {
            var all = await _connection.Table<EmployeeModel>().ToListAsync();

            if (await _connection.Table<EmployeeModel>().Where(i => i.Id == item.Id).CountAsync() > 0)
            {
                var itemToUpdate = await _connection.Table<EmployeeModel>().FirstOrDefaultAsync(i => i.Id == item.Id);
                itemToUpdate.Name = item.Name;
                itemToUpdate.Password = item.Password;
                return await _connection.UpdateAsync(itemToUpdate);
            }

            return await _connection.InsertAsync(item);
        }

        public Task<int> DeleteItemAsync(EmployeeModel item)
        {
            return _connection.DeleteAsync(item);
        }

        #endregion
    }
}


