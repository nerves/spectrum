﻿
using System;
using System.Threading.Tasks;
using FFImageLoading;
using FFImageLoading.Svg.Platform;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using Storage.Models;
using UIKit;

namespace ios.Views.Cells
{
    public partial class EmployeeTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("EmployeeTableViewCell");
        public static readonly UINib Nib;

        static EmployeeTableViewCell()
        {
            Nib = UINib.FromName("EmployeeTableViewCell", NSBundle.MainBundle);
        }

        protected EmployeeTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.

            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<EmployeeTableViewCell, EmployeeModel>();
                set.Bind(TitleLabel).To(m => m.Name);
                set.Bind(SubtitleLabel).To(m => m.Password);
                set.Apply();
            });


            var deleteIcon = @"<svg width=""100"" height=""100"" viewBox =""0 0 24 24"" ><path fill=""white"" d =""M9 11.75c-.69 0-1.25.56-1.25 1.25s.56 1.25 1.25 1.25 1.25-.56 1.25-1.25-.56-1.25-1.25-1.25zm6 0c-.69 0-1.25.56-1.25 1.25s.56 1.25 1.25 1.25 1.25-.56 1.25-1.25-.56-1.25-1.25-1.25zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8 0-.29.02-.58.05-.86 2.36-1.05 4.23-2.98 5.21-5.37C11.07 8.33 14.05 10 17.42 10c.78 0 1.53-.09 2.25-.26.21.71.33 1.47.33 2.26 0 4.41-3.59 8-8 8z"" /></svg>";

            Task.Run(async () =>
            {
                UIImage image = await ImageService.Instance
                    .LoadString(deleteIcon)
                    .WithCustomDataResolver(new SvgDataResolver(64, 0, true))
                    .AsUIImageAsync();


                BeginInvokeOnMainThread(() =>
                {
                    FaceImageView.Image = image;
                });
            });
        }


    }
}
