﻿
using System.Threading.Tasks;
using FFImageLoading;
using FFImageLoading.Svg.Platform;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using MVXTest.ViewModels;
using UIKit;

namespace ios.Views
{
    public partial class EmployeeDetailViewController : MvxViewController<EmployeeDetailsViewModel>
    {
        public EmployeeDetailViewController() : base("EmployeeDetailViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // NOTE: Both PasswordEntry and ConfirmPasswordEntry have been left as regular TextViews instead of Security Text Views for demo purposes
            var set = this.CreateBindingSet<EmployeeDetailViewController, EmployeeDetailsViewModel>();

            set.Bind(NameEntry)
                .To(vm => vm.UserName);

            set.Bind(PasswordEntry)
                .To(vm => vm.Password);

            set.Bind(NameEntry)
                .To(vm => vm.UserName);

            set.Bind(ConfirmPasswordEntry)
                .To(vm => vm.PasswordConfirm);

            set.Bind(LengthLabel)
                .To(vm => vm.LengthLabelText);

            set.Bind(NoRepeatLabel)
                .To(vm => vm.NoRepeatLabelText);

            set.Bind(NoSpecialLabel)
                .To(vm => vm.NoSpecialLabelText);
            set.Apply();

            set.Bind(NumAndLetLabel)
                .To(vm => vm.NumAndLetLableText);
            set.Apply();

            set.Bind(MatchLabel)
                .To(vm => vm.MatchLableText);
            set.Apply();

            set.Bind(SaveEmployeeButton).To(vm => vm.SaveEmployeeCommand);

            set.Bind(DeleteEmployeeButton).To(vm => vm.DeleteEmployeeCommand);
            set.Apply();

            Task.Run(async () =>
            {
                var deleteIcon = @"<svg width=""24"" height=""24"" viewBox =""0 0 24 24"" ><path fill=""white"" d =""M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v10zM18 4h-2.5l-.71-.71c-.18-.18-.44-.29-.7-.29H9.91c-.26 0-.52.11-.7.29L8.5 4H6c-.55 0-1 .45-1 1s.45 1 1 1h12c.55 0 1-.45 1-1s-.45-1-1-1z"" /></svg>";
                var saveIcon = @"<svg viewBox=""0 0 24 24"" width=""100"" height=""100""><path fill=""white"" d=""M17.59 3.59c-.38-.38-.89-.59-1.42-.59H5c-1.11 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V7.83c0-.53-.21-1.04-.59-1.41l-2.82-2.83zM12 19c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm1-10H7c-1.1 0-2-.9-2-2s.9-2 2-2h6c1.1 0 2 .9 2 2s-.9 2-2 2z""/></svg> ";
                UIImage saveImage = await ImageService.Instance
                    .LoadString(saveIcon)
                    .WithCustomDataResolver(new SvgDataResolver(64, 0, true))
                    .AsUIImageAsync();
                UIImage deleteImage = await ImageService.Instance
                  .LoadString(deleteIcon)
                  .WithCustomDataResolver(new SvgDataResolver(64, 0, true))
                  .AsUIImageAsync();

                BeginInvokeOnMainThread(() =>
                {
                    SaveEmployeeButton.ContentEdgeInsets = new UIEdgeInsets(8, 8, 8, 8);
                    SaveEmployeeButton.SetImage(saveImage, UIControlState.Normal);

                    DeleteEmployeeButton.ContentEdgeInsets = new UIEdgeInsets(8, 8, 8, 8);
                    DeleteEmployeeButton.SetImage(deleteImage, UIControlState.Normal);
                });
            });

            View.AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                if (ConfirmPasswordEntry.IsFirstResponder)
                {
                    this.ConfirmPasswordEntry.ResignFirstResponder();
                }

                if (PasswordEntry.IsFirstResponder)
                {
                    this.PasswordEntry.ResignFirstResponder();
                }

                if (NameEntry.IsFirstResponder)
                {
                    this.NameEntry.ResignFirstResponder();
                }
            }));
        }
    }
}

