﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ios.Views
{
    [Register ("EmployeeDetailViewController")]
    partial class EmployeeDetailViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ConfirmPasswordEntry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DeleteEmployeeButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LengthLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MatchLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField NameEntry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NoRepeatLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NoSpecialLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NumAndLetLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordEntry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaveEmployeeButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ConfirmPasswordEntry != null) {
                ConfirmPasswordEntry.Dispose ();
                ConfirmPasswordEntry = null;
            }

            if (DeleteEmployeeButton != null) {
                DeleteEmployeeButton.Dispose ();
                DeleteEmployeeButton = null;
            }

            if (LengthLabel != null) {
                LengthLabel.Dispose ();
                LengthLabel = null;
            }

            if (MatchLabel != null) {
                MatchLabel.Dispose ();
                MatchLabel = null;
            }

            if (NameEntry != null) {
                NameEntry.Dispose ();
                NameEntry = null;
            }

            if (NoRepeatLabel != null) {
                NoRepeatLabel.Dispose ();
                NoRepeatLabel = null;
            }

            if (NoSpecialLabel != null) {
                NoSpecialLabel.Dispose ();
                NoSpecialLabel = null;
            }

            if (NumAndLetLabel != null) {
                NumAndLetLabel.Dispose ();
                NumAndLetLabel = null;
            }

            if (PasswordEntry != null) {
                PasswordEntry.Dispose ();
                PasswordEntry = null;
            }

            if (SaveEmployeeButton != null) {
                SaveEmployeeButton.Dispose ();
                SaveEmployeeButton = null;
            }
        }
    }
}