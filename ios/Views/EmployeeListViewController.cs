﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FFImageLoading;
using FFImageLoading.Svg.Platform;
using Foundation;
using ios.Views.Cells;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using MVXTest.ViewModels;
using Storage.Models;
using UIKit;

namespace ios.Views
{
    public partial class EmployeeListViewController : MvxViewController<EmployeeListViewModel>
    {
        private UIRefreshControl _refreshControl;

        public EmployeeListViewController() : base("EmployeeListViewController", null)
        {

        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            Refresh();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.Title = "Employees";

            var tableViewSource = new MvxSimpleTableViewSource(EmployeeListTableView, nameof(EmployeeTableViewCell), EmployeeTableViewCell.Key)
            {
                DeselectAutomatically = true
            };
            EmployeeListTableView.RowHeight = 120;

            var set = this.CreateBindingSet<EmployeeListViewController, EmployeeListViewModel>();
            set.Bind(tableViewSource).To(vm => vm.Items);

            set.Bind(AddEmployeeButton).To(vm => vm.AddNewEmployeeCommand);
            set.Apply();

            EmployeeListTableView.Source = tableViewSource;
            EmployeeListTableView.ReloadData();

            this.CreateBinding(tableViewSource).For(s => s.SelectionChangedCommand).To<EmployeeListViewModel>(vm => vm.ItemSelectedCommand).Apply();

            // Create the UIRefreshControl
            _refreshControl = new UIRefreshControl();

            // Handle the pullDownToRefresh event
            _refreshControl.ValueChanged += (object sender, EventArgs e) =>
            {
                Refresh();
            };

            // Add the UIRefreshControl to the TableView
            EmployeeListTableView.AddSubview(_refreshControl);


            Task.Run(async () =>
            {
                var addIcon = @"<svg x=""0px"" y =""0px"" width =""100px"" height =""100px"" viewBox =""0 0 24 24"" enable-background=""new 0 0 24 24"" xml:space=""preserve"" > <path fill=""white"" d =""M18,13h-5v5c0,0.55-0.45,1-1,1h0c-0.55,0-1-0.45-1-1v-5H6c-0.55,0-1-0.45-1-1v0c0-0.55,0.45-1,1-1h5V6c0-0.55,0.45-1,1-1h0 c0.55,0,1,0.45,1,1v5h5c0.55,0,1,0.45,1,1v0C19,12.55,18.55,13,18,13z"" /></svg>";

                UIImage addImage = await ImageService.Instance
                    .LoadString(addIcon)
                    .WithCustomDataResolver(new SvgDataResolver(64, 0, true))
                    .AsUIImageAsync();

                BeginInvokeOnMainThread(() =>
                {
                    AddEmployeeButton.ContentEdgeInsets = new UIEdgeInsets(8, 8, 8, 8);
                    AddEmployeeButton.SetImage(addImage, UIControlState.Normal);
                });
            });
        }

        private void Refresh()
        {
            ViewModel.ReloadDataCommand.Execute(null);
            EmployeeListTableView.ReloadData();
            _refreshControl.EndRefreshing();
        }

        public class TableSource : MvxTableViewSource
        {
            private static readonly NSString EmployeeCellIdentifier = new NSString("EmployeeCell");

            public TableSource(UITableView tableView)
                : base(tableView)
            {
                tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
                tableView.RegisterNibForCellReuse(UINib.FromName("EmployeeCell", NSBundle.MainBundle), EmployeeCellIdentifier);
            }

            protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
            {

                return (UITableViewCell)TableView.DequeueReusableCell(EmployeeCellIdentifier, indexPath);
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                return base.GetCell(tableView, indexPath);
            }
        }
    }
}

