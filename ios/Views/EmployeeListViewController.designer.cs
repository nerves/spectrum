﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ios.Views
{
    [Register ("EmployeeListViewController")]
    partial class EmployeeListViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddEmployeeButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView EmployeeListTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AddEmployeeButton != null) {
                AddEmployeeButton.Dispose ();
                AddEmployeeButton = null;
            }

            if (EmployeeListTableView != null) {
                EmployeeListTableView.Dispose ();
                EmployeeListTableView = null;
            }
        }
    }
}